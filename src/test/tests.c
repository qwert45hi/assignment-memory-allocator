#include "tests.h"

#include <malloc.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "../mmm/mem.h"
#include "../mmm/mem_internals.h"
#include "../mmm/util.h"

/* Вспомогательное */
static _Noreturn void test_err(int test_no, const char* msg, ...) {
    va_list args;
    va_start(args, msg);
    fprintf(stderr, "ERROR in TEST#%d: ", test_no);
    vfprintf(stderr, msg, args);
    fprintf(stderr, "\n");
    va_end(args);
    abort();
}

static void test_starting(int test_no) { fprintf(stdout, "\nStarting TEST#%d...\n", test_no); }
static void test_passed(int test_no) { fprintf(stdout, "TEST#%d PASSED!\n", test_no); }

static bool malloc_and_check(size_t query, void** test) {
    *test = _malloc(query);
    return *test != NULL;
}

static bool double_malloc_and_check(size_t query, int** test, int** real) {
    *real = malloc(query);
    return malloc_and_check(query, (void**)test);
}

static bool check_match(int* test, int* real, size_t len) {
    for (size_t i = 0; i < len; i++) {
        if (test[i] != real[i]) return false;
    }
    return true;
}

static struct block_header* find_header(void* ptr) {
    return (struct block_header*)((uint8_t*)ptr - offsetof(struct block_header, contents));
}

/* Обычное успешное выделение памяти */
void test1(struct block_header* test_heap) {
    test_starting(1);
    debug_heap(stdout, test_heap);

    int* test = NULL;
    int* real = NULL;
    if (!double_malloc_and_check(1000, &test, &real)) test_err(1, "failed to allocate the block");
    debug_heap(stdout, test_heap);

    test[0] = real[0] = 54;
    test[1] = real[1] = 0;
    test[2] = real[2] = -43;
    if (!check_match(test, real, 3)) test_err(1, "data missmatch in blocks");

    _free(test);
    free(real);
    debug_heap(stdout, test_heap);

    test_passed(1);
}

/* Освобождение одного блока из нескольких выделенных */
void test2(struct block_header* test_heap) {
    test_starting(2);
    debug_heap(stdout, test_heap);

    void* block1 = NULL;
    void* block2 = NULL;
    if (!malloc_and_check(1024, &block1)) test_err(2, "failed to allocate block#1");
    if (!malloc_and_check(1024, &block2)) test_err(2, "failed to allocate block#2");
    debug_heap(stdout, test_heap);

    _free(block1);
    debug_heap(stdout, test_heap);

    _free(block2);
    debug_heap(stdout, test_heap);

    test_passed(2);
}

/* Освобождение двух блоков из нескольких выделенных */
void test3(struct block_header* test_heap) {
    test_starting(3);
    debug_heap(stdout, test_heap);

    void* block1 = NULL;
    void* block2 = NULL;
    void* block3 = NULL;
    if (!malloc_and_check(1024, &block1)) test_err(3, "failed to allocate block#1");
    if (!malloc_and_check(1024, &block2)) test_err(3, "failed to allocate block#2");
    if (!malloc_and_check(1024, &block3)) test_err(3, "failed to allocate block#3");
    debug_heap(stdout, test_heap);

    _free(block2);
    _free(block3);
    debug_heap(stdout, test_heap);

    _free(block1);
    debug_heap(stdout, test_heap);

    test_passed(3);
}

/* Память закончилась, новый регион памяти расширяет старый */
void test4(struct block_header* test_heap) {
    test_starting(4);
    debug_heap(stdout, test_heap);

    void* block1 = NULL;
    void* block2 = NULL;
    if (!malloc_and_check(32768, &block1)) test_err(4, "failed to allocate block#1");
    if (!malloc_and_check(32768, &block2)) test_err(4, "failed to allocate block#2");
    debug_heap(stdout, test_heap);

    struct block_header* header1 = find_header(block1);
    if ((uint8_t*)header1->contents + header1->capacity.bytes != (uint8_t*)(find_header(block2)))
        test_err(4, "block#2 is not next to block#1");

    _free(block2);
    _free(block1);
    debug_heap(stdout, test_heap);

    test_passed(4);
}

/* Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона
 * адресов, новый регион выделяется в другом месте */
void test5(struct block_header* test_heap) {
    test_starting(5);
    debug_heap(stdout, test_heap);

    void* block1 = NULL;
    if (!malloc_and_check(test_heap->capacity.bytes - 1, &block1))
        test_err(5, "failed to allocate block#1");
    debug_heap(stdout, test_heap);

    struct block_header* last = test_heap;
    while (last->next != NULL) last = last->next;
    last = (struct block_header*)((uint8_t*)last + size_from_capacity(last->capacity).bytes);
    alloc_region(last, 10000);

    void* block2 = NULL;
    if (!malloc_and_check(test_heap->capacity.bytes - 1, &block2))
        test_err(5, "failed to allocate block#2");
    debug_heap(stdout, test_heap);

    // printf("0x%" PRIXPTR "\n", (uintptr_t)last);

    if (find_header(block2) == last) test_err(5, "block#2 initialized next to block#1");

    // freeing
    debug_heap(stdout, test_heap);

    test_passed(5);
}

void test_all() {
    struct block_header* test_heap = heap_init(32768);
    test1(test_heap);
    test2(test_heap);
    test3(test_heap);
    test4(test_heap);
    test5(test_heap);
}
