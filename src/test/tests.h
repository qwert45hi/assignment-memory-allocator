#ifndef TESTS_H
#define TESTS_H

#include "mmm/mem_internals.h"

void test1(struct block_header* test_heap);
void test2(struct block_header* test_heap);
void test3(struct block_header* test_heap);
void test4(struct block_header* test_heap);
void test5(struct block_header* test_heap);

void test_all();

#endif